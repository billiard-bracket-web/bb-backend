package com.frosse.billiard.tournament;

import com.frosse.billiard.player.Player;
import com.frosse.billiard.player.PlayerRepository;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class TournamentService {
    private TournamentRepository tournamentRepository;
    private PlayerRepository playerRepository;

    public TournamentService(TournamentRepository tournamentRepository, PlayerRepository playerRepository) {
        this.tournamentRepository = tournamentRepository;
        this.playerRepository = playerRepository;
    }
    public void addTournament(Integer[] players) {
        Tournament tournament = new Tournament();
        Set<Player> playerSet = new HashSet<>();
        for (Integer p : players) {
            Player player = playerRepository.findById(p).orElse(null);
            playerSet.add(player);
        }
        tournament.setPlayers(playerSet);
        tournamentRepository.save(tournament);
    }
    public Iterable<Tournament> findAll() {
        return tournamentRepository.findAll();
    }
    public Optional<Tournament> getById(Integer id ) {
        return tournamentRepository.findById(id);
    }

}
