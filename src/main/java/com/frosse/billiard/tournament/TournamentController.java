package com.frosse.billiard.tournament;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@RestController
@RequestMapping("api/v1/tournament")
public class TournamentController {
    private TournamentService tournamentService;

    public TournamentController(TournamentService tournamentService) {
        this.tournamentService = tournamentService;
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/")
    public @ResponseBody Iterable<Tournament> getAllTournaments() {
        return tournamentService.findAll();
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/add")
    public void addNewTournament(@RequestBody Integer[] players) {
        tournamentService.addTournament(players);
    }
    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(value = "/{id}")
    public Optional<Tournament> getById(@PathVariable Integer id) {
        return tournamentService.getById(id);
    }
}
