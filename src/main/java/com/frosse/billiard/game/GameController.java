package com.frosse.billiard.game;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/game")
public class GameController {

    private GameService gameService;

    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @GetMapping("/")
    public @ResponseBody Iterable<Game> getAllGames() {
        return gameService.getAllGames();
    }

    @PostMapping("/add")
    public void addGame(
            @JsonProperty("score1") Integer score1,
            @JsonProperty("score2") Integer score2,
            @JsonProperty("player1") Integer player1,
            @JsonProperty("player2") Integer player2) {
        gameService.addGame(score1, score2, player1, player2);
    }

    @DeleteMapping(value = "/{id}")
    public void deleteGame(@PathVariable Integer id) {
        gameService.deleteGame(id);
    }
}
