package com.frosse.billiard.game;

import com.frosse.billiard.player.Player;

import javax.persistence.*;

@Entity
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private int score1;
    private int score2;

    @ManyToOne
    @JoinColumn(name = "player1")
    private Player player1;
    @ManyToOne
    @JoinColumn(name = "player2")
    private Player player2;

    public Game() {

    }
    public Game(int score1, int score2, Player player1, Player player2) {
        this.score1 = score1;
        this.score2 = score2;
        this.player1 = player1;
        this.player2 = player2;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getScore1() {
        return score1;
    }

    public void setScore1(int score1) {
        this.score1 = score1;
    }

    public int getScore2() {
        return score2;
    }

    public void setScore2(int score2) {
        this.score2 = score2;
    }

    public Player getPlayer1() {
        return player1;
    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }
}
