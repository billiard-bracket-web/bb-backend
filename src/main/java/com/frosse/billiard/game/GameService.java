package com.frosse.billiard.game;

import com.frosse.billiard.player.PlayerRepository;
import org.springframework.stereotype.Service;

@Service
public class GameService {

    private GameRepository gameRepository;
    private PlayerRepository playerRepository;

    public GameService(GameRepository gameRepository, PlayerRepository playerRepository) {
        this.gameRepository = gameRepository;
        this.playerRepository = playerRepository;
    }

    public void addGame(int score1, int score2, int player1, int player2) {
        Game game = new Game();
        game.setPlayer1(playerRepository.findById(player1).orElse(null));
        game.setPlayer2(playerRepository.findById(player2).orElse(null));
        game.setScore1(score1);
        game.setScore2(score2);
        gameRepository.save(game);
    }

    public Iterable<Game> getAllGames() {
        return gameRepository.findAll();
    }

    public void deleteGame(Integer id) {
        gameRepository.deleteById(id);
    }

    public void addGame(Game game) {
        gameRepository.save(game);
    }
}
