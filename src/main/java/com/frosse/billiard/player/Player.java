package com.frosse.billiard.player;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.frosse.billiard.tournament.Tournament;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Player {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String firstName;
    private String lastName;
    private int tasoitus;

    @ManyToMany(mappedBy = "players")
    private Set<Tournament> tournaments;

    @JsonIgnoreProperties("players")
    public Set<Tournament> getTournaments() {
        return tournaments;
    }

    public void setTournaments(Set<Tournament> tournaments) {
        this.tournaments = tournaments;
    }

    public Player() {

    }

    public Player(String firstName, String lastName, int tasoitus) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.tasoitus = tasoitus;
        this.tournaments = new HashSet<>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getTasoitus() {
        return tasoitus;
    }

    public void setTasoitus(int tasoitus) {
        this.tasoitus = tasoitus;
    }


}
