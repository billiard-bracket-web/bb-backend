package com.frosse.billiard.player;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PlayerService {

    private PlayerRepository playerRepository;

    public PlayerService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public void addPlayer(Player player) {
        playerRepository.save(player);
    }

    public Iterable<Player> findAll() {
        return playerRepository.findAll();
    }

    public Optional<Player> getById(Integer id) {
        return playerRepository.findById(id);
    }


    public void deletePlayer(Integer id) {
        playerRepository.deleteById(id);
    }
}
