package com.frosse.billiard.player;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("api/v1/player")
public class PlayerController {

    private PlayerService playerService;

    public PlayerController(PlayerService playerService) {
        this.playerService = playerService;
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/")
    public @ResponseBody Iterable<Player> getAllPlayers() {
        return playerService.findAll();
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(value = "/{id}")
    public Optional<Player> getById(@PathVariable Integer id) {
        return playerService.getById(id);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/add")
    public void addNewPlayer(@RequestBody Player player) {
        playerService.addPlayer(player);
    }

    @DeleteMapping(value = "/{id}")
    public void deletePlayer(@PathVariable Integer id) {
        playerService.deletePlayer(id);
    }

}
